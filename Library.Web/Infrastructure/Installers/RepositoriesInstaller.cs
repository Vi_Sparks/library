﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Library.Core.Entities;
using Library.Data.Interfaces.Base;
using Library.Data;

namespace Library.Web.Infrastructure.Installers
{
    public class RepositoriesInstaller: IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For(typeof(IRepository<>)).ImplementedBy(typeof(IRepository<>)).LifeStyle.PerWebRequest,
                Component.For(typeof(IDataContext)).ImplementedBy<DataContext>().LifeStyle.PerWebRequest,
                Component.For(typeof(IRepository<Book>)).ImplementedBy(typeof(Repository<Book>)).LifeStyle.PerWebRequest,
                Component.For(typeof(IRepository<Author>)).ImplementedBy(typeof(Repository<Author>)).LifeStyle.PerWebRequest);
        }
    }
}
