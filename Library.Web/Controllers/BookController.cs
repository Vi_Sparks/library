﻿using System;
using System.Web.Mvc;
using System.Linq;
using System.Web;
using System.Net.Http;
using Library.Core.Entities;
using Library.Data.Interfaces.Services;
using Library.View.Models;

namespace Library.Web.Controllers
{
    public class BookController : Controller
    {
        protected readonly IBookServices bookServices;

        public BookController(IBookServices bookServices)
        {
            this.bookServices = bookServices;
        }

        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult CreateBook()
        {
            return PartialView("CreateBook");
        }

        [HttpPost]
        public ActionResult CreateBook(BookViewModel book)
        {
            if (ModelState.IsValid)
            {
                
            }

            return PartialView("CreateBook");
        }


        [HttpPost]
        public JsonResult UpdateBook(long id)
        {
            HttpRequest request = System.Web.HttpContext.Current.Request;

            var response = new HttpResponseMessage();

            Book book = this.bookServices.GetBookById(id);

            try
            {

                if (book != null)
                {
                    book.Title = string.IsNullOrEmpty(Request.Form["Title"]) ? book.Title : Request.Form["Title"];

                    book.Genre = string.IsNullOrEmpty(Request.Form["Genre"]) ? book.Genre : Request.Form["Genre"];

                    book.Year = string.IsNullOrEmpty(Request.Form["Year"]) ? book.Year : Int32.Parse((Request.Form["Year"]));

                    book.Price = string.IsNullOrEmpty(Request.Form["Price"]) ? book.Price : Single.Parse((Request.Form["Price"]));

                    book.Content = string.IsNullOrEmpty(Request.Form["Content"]) ? book.Content : Request.Form["Content"];

                    book.CountOfPages = string.IsNullOrEmpty(Request.Form["CountOfPages"]) ? book.CountOfPages : Int32.Parse((Request.Form["CountOfPages"]));

                    bookServices.UpdateBook(id, book);

                    bookServices.SaveBooks();

                    var jsondata = BookViewModel.CreateBookViewModel(book);

                    return Json(jsondata);
                }

                else
                {
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    response.Content = new StringContent(string.Format("The book with id {0} was not found in the database"));
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.Content = new StringContent(string.Format("There was an error updating book {0}"));
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public void DeleteBook(long id)
        {
                this.bookServices.DeleteBook(id);
                this.bookServices.SaveBooks();
        }

       

        public JsonResult ListOfBooks()
        {
            var jsondata = (from o in bookServices.GetBooksWithAuthors() 
                            select new BookViewModel(o));

            return Json(jsondata, JsonRequestBehavior.AllowGet);
        }

    }
}