﻿using System.Web.Mvc;
using Library.Data.Interfaces.Services;

namespace Library.Web.Controllers
{
    public class HomeController : Controller
    {
        protected readonly IBookServices bookServices;

        public HomeController(IBookServices bookServices)
        {
            this.bookServices = bookServices;
        }

        public ActionResult Index()
        {
            return View("Index");
        }
    }
}