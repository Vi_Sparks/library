﻿$(document).ready(function () {
    $(document).on("click", "#contentPager a", function () {
        $.ajax({
            url: $(this).attr("href"),
            type: 'GET',
            cache: false,
            success: function (result) {
                $('#books').html(result);
            }
        });
        return false;
    });
});