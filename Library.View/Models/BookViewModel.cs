﻿using System.Collections.Generic;
using System.Linq;
using Library.Core.Entities;

namespace Library.View.Models
{
    public class BookViewModel : BaseEntity
    {
        public int CountOfPages { get; set; }

        public string Title { get; set; }

        public int Year { get; set; }

        public float Price { get; set; }

        public string Genre { get; set; }

        public string Content { get; set; }

        public IEnumerable<string> Authors { get; set; }

        public BookViewModel()
        {

        }

        public BookViewModel (Book book)
        {
            Id = book.Id;
            Title = book.Title;
            Price = book.Price;
            Name = book.Name;
            Year = book.Year;
            Authors = book.Authors.Select(author => author.Name);
            Genre = book.Genre;
            CountOfPages = book.CountOfPages;
            Content = book.Content;

        }

        public static BookViewModel CreateBookViewModel(Book book)
        {
            BookViewModel newBook = new BookViewModel()
            {
                Id = book.Id,
                Title = book.Title,
                Price = book.Price,
                Name = book.Name,
                Year = book.Year,
                Authors = book.Authors.Select(author => author.Name),
                Genre = book.Genre,
                CountOfPages = book.CountOfPages,
                Content = book.Content
            };

            return newBook;

        }
    }
}
