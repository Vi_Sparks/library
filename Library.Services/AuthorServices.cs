﻿using System;
using System.Collections.Generic;
using NLog;
using Library.Core.Entities;
using Library.Data.Interfaces.Services;
using Library.Data.Interfaces.Base;
namespace Library.Services
{
    public class AuthorServices : IAuthorServices
    {
        private readonly IRepository<Author> authorsRepository;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public AuthorServices(IRepository<Author> authorsRepository)
        {
            this.authorsRepository = authorsRepository;
        }

        public Author CreateAuthor(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("The name parameter in the CreateAuthor method cannot be null or empty!", "name");
            }

            else if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("The name parameter in the CreateAuthor method cannot be null or white space!", "name");
            }

            return new Author {  Name = name };
        }

        public Author AddAuthor(Author Author)
        {
            try
            {
                return authorsRepository.Add(Author);
            }

            catch (ArgumentNullException exception)
            {
                logger.Error(exception, "Error in AddAuthor method.");
                return null;
            }
        }

        public bool UpdateAuthor(long id, Author newAuthor)
        {
            Author author = GetAuthorById(id);

            if (author != null)
            {

                if (!authorsRepository.Edit(author))
                {
                    logger.Error("Error in UpdateAuthorByName method.");
                    return false;
                }
            }

            return true;

        }

        public bool DeleteAuthor(long id)
        {
            Author Author = GetAuthorById(id);

            if (Author != null)
            {
                if (!authorsRepository.Delete(Author))
                {
                    logger.Error("Error in Delete method.");
                    return false;
                }

                return true;
            }

            return false;
        }

        public Author GetAuthorById(long id)
        {
            Author author = authorsRepository.GetById(id);

            if (author == null)
            {
                logger.Error("Error in GetAuthorById method.");
            }

            return author;
        }

        public Author GetAuthorByName(string name)
        {
            Author Author = authorsRepository.GetByName(name);

            if (Author == null)
            {
                logger.Error("Error in GetAuthorByName method.");
            }

            return Author;
        }

        public IEnumerable<Author> GetAllAuthors()
        {
            IEnumerable<Author> Authors = authorsRepository.GetAll();

            if (Authors == null)
            {
                logger.Error("Error in GetAuthorByName method.");
            }

            return Authors;
        }

        public bool SaveAuthors()
        {
            try
            {
                authorsRepository.SaveChanges();
            }

            catch (Exception exception)
            {
                logger.Error(exception, "Error in SaveAuthors method.");
                throw;
            }

            return true;

        }
    }
}
