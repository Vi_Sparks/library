﻿using System.Data.Entity.ModelConfiguration;
using Library.Core.Entities;

namespace Library.Data.Configurations
{
    public class AuthorConfiguration : EntityTypeConfiguration<Author>
    {
        public AuthorConfiguration()
        {
            HasKey(author => author.Id);
            HasMany(author => author.Books).WithMany(book => book.Authors);
        }
    }
}
