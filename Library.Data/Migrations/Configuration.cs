using System.Collections.Generic;
using Library.Core.Entities;
namespace Library.Data.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Library.Data.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Library.Data.DataContext";
        }

        protected override void Seed(Library.Data.DataContext context)
        {
            context.Set<Author>().AddRange( new List<Author>()
            {
                new Author() { Name = "Jane Austen" },
                new Author() { Name = "Charles Dickens" },
                new Author() { Name = "Miguel de Cervantes" }
            }
                                           );

            context.Set<Book>().AddRange( new List<Book>()
            {
                new Book()
                    {
                        Title = "Pride and Prejudice",
                        Year = 1813,
                        Price = 9.99f,
                        Genre = "Comedy of manners"
                    },

                new Book()
                    {
                        Title = "Northanger Abbey",
                        Year = 1817,
                        Price = 12.95f,
                        Genre = "Gothic parody"
                    },

                new Book()
                    {
                       Title = "Don Quixote",
                       Year = 1617,
                       Price = 8.95f,
                       Genre = "Picaresque"
                    }
            }

          );

        }

    }

}

