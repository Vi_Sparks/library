﻿using System.Collections.Generic;
using Library.Core.Entities;

namespace Library.Data.Interfaces.Services
{
    public interface IBookServices
    {
        Book CreateBook(ICollection<Author> authors, string name);

        Book CreateBook(Book book);

        Book AddBook(Book book);

        Book UpdateBook(long id, Book newook);

        bool DeleteBook(long id);

        Book GetBookById(long id);

        Book GetBookByName(string name);

        IEnumerable<Book> GetAllBooks();

        IEnumerable<Author> GetAuthorsByBook(Book book);

        IEnumerable<Book> GetBooksWithAuthors();

        bool SaveBooks();
    }
}
