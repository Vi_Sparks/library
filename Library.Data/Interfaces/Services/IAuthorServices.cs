﻿using System.Collections.Generic;
using Library.Core.Entities;

namespace Library.Data.Interfaces.Services
{
    public interface IAuthorServices
    {
        Author CreateAuthor(string name);

        Author AddAuthor(Author author);

        Author GetAuthorById(long id);

        Author GetAuthorByName(string name);

        bool UpdateAuthor(long id, Author newAuthor);

        bool DeleteAuthor(long id);

        IEnumerable<Author> GetAllAuthors();

        bool SaveAuthors();

    }
}
