﻿using System;
using System.Data.Entity;
using System.Linq;
using NLog;
using Library.Core.Entities;
using Library.Data.Interfaces.Base;

namespace Library.Data
{
    public class Repository <T>: IRepository<T> where T : BaseEntity, new()
    {
        
        protected readonly IDataContext dataContext;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private DbSet<T> CurrentSet
        {
            get
            {
                return this.dataContext.Set<T>();
            }
        }

        public Repository(IDataContext context)
        {
            this.dataContext = context;
        }


        public IQueryable<T> GetAll()
        {
            return this.CurrentSet.AsQueryable();
        }

        public T Add(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("The entity parameter in the Add method cannot be null!", "entity");
            }

            this.CurrentSet.Add(entity);

            return entity;
        }

        public bool Edit(T entity)
        {
                try
                {
                    dataContext.Entry(entity).State = EntityState.Modified;
                }

                catch (Exception exception)
                {
                    logger.Error(exception, "Failed to edit the item.");
                    return false;
                }

            return true;
        }

        public bool Delete(T entity)
        {
            try
            {
                this.CurrentSet.Remove(entity);
            }

            catch (Exception exception)
            {
                logger.Error(exception, "Failed to delete the item.");
                return false;
            }

            return true;
        }

        public bool SaveChanges()
        {
            try
            {
                this.dataContext.SaveChanges();
            }

            catch (Exception exception)
            {
                logger.Error(exception, "Failed to save changes to the database.");
                return false;
            }

            return true;
        }

        public T GetById(Int64 id)
        {
            T item = new T();

            try
            {
                item = this.CurrentSet.Find(id);
            }

            catch (InvalidOperationException exception)
            {
                logger.Error(exception, "Failed to get the item.");
                return null;
            }

            return item;
        }

        public T GetByName(string name)
        {
            T item = new T();

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("The name parameter in the GetByName method cannot be null or empty!", "name");
            }

            else if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("The name parameter in the GetByName method cannot be null or white space!", "name");
            }

            try
            {
                item = GetAll().First(entity => entity.Name.Equals(name));
            }

            catch (Exception exception)
            {
                logger.Error(exception, "Failed to get all the items from repository.");
                return null;
            }

            return item;
        }

    }
}

