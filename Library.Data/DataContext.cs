﻿using System.Data.Entity;
using Library.Data.Interfaces.Base;
using Library.Data.Configurations;

namespace Library.Data
{
    public class DataContext : DbContext, IDataContext
    {
            public DataContext() : base("LibraryDatabase")
            {

            }

            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                modelBuilder.Configurations.Add(new BookConfiguration());
                modelBuilder.Configurations.Add(new AuthorConfiguration());
                base.OnModelCreating(modelBuilder);
            }
        }
    }

