﻿namespace Library.Core.Entities
{
    public class BaseEntity
    {
        public long Id { get; protected set; }

        public string Name { get; set; }
    }
}
