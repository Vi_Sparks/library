﻿using System.Collections.Generic;

namespace Library.Core.Entities
{
    public class Book : BaseEntity
    {
        #region Data

        public int CountOfPages { get; set; }
      
        public string Title { get; set; }
       
        public int Year { get; set; }
        
        public float Price { get; set; }
        
        public string Genre { get; set; }

        public string Content { get; set; }

        #endregion

        #region Relationships

        public virtual ICollection<Author> Authors { get; set; }

        #endregion

    }
}
